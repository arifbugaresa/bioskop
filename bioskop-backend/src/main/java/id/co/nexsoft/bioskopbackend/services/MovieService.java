package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.dto.requests.MovieRequest;
import id.co.nexsoft.bioskopbackend.dto.respons.MovieDto;
import id.co.nexsoft.bioskopbackend.entities.Movie;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import id.co.nexsoft.bioskopbackend.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public ResponseEntity<?> addMovie(MovieRequest movieRequest) {
        movieRequest.setDuration(2);
        movieRepository.saveMovie(movieRequest);
        List<MovieDto> movies = movieRepository.findAllMovie();
        return ResponseEntity.ok()
                .body(movies);
    }

    public ResponseEntity<?> findMovie(int id) {
        Optional<Movie> movieRepo = Optional.ofNullable(movieRepository.findById(id));
        if (!movieRepo.isPresent())
            return ResponseEntity.notFound().build();

        MovieDto movie = movieRepository.findMovie(id);
        return ResponseEntity.ok()
                .body(movie);
    }

    public ResponseEntity<?> updateMovie(int id, Movie movie) {
        Optional<Movie> movieRepo = Optional.ofNullable(movieRepository.findById(id));
        if (!movieRepo.isPresent())
            return ResponseEntity.notFound().build();

        movie.setDuration(2);
        movieRepository.updateMovie(id, movie);

        List<MovieDto> listMovie = movieRepository.findAllMovie();
        return ResponseEntity.ok()
                .body(listMovie);
    }

    public ResponseEntity<?> findAllMovie() {
        List<MovieDto> movies = movieRepository.findAllMovie();
        return ResponseEntity.ok()
                .body(movies);
    }

    // admin get movie paginated
    public ResponseEntity<?> findMoviePaginated(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<MovieDto> movies = movieRepository.findMoviePaginated(pageable);

        return ResponseEntity.ok()
                .body(movies);
    }
}
