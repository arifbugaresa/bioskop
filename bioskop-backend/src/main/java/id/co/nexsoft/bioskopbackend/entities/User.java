package id.co.nexsoft.bioskopbackend.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "phone_number"),
                @UniqueConstraint(columnNames = "email")
        })
public class User {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @NotBlank
    @NotNull
    private String name;

    @NotBlank
    @NotNull
    @Size( max = 20)
    private String username;

    @NotBlank
    @NotNull
    private String password;

    @NotBlank
    @NotNull
    private String address;

    @NotBlank
    @NotNull
    private String phone_number;

    @NotBlank
    @NotNull
    @Email
    private String email;

    @NotNull
    private LocalDate created_at;

    private LocalDate deleted_at;

    @NotBlank
    @NotNull
    private String role;

    public User( String name,
                 String username,
                 String password,
                 String address,
                 String phone_number,
                 String email,
                 LocalDate created_at,
                 String role ) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.address = address;
        this.phone_number = phone_number;
        this.email = email;
        this.created_at = created_at;
        this.role = role;
    }
}
