package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.dto.requests.TicketRequest;
import id.co.nexsoft.bioskopbackend.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @GetMapping("/ticket")
    public ResponseEntity<?> findAllTicket() {
        return ticketService.findAllTicket();
    }

    @GetMapping("/ticket/{id}")
    public ResponseEntity<?> findTicketByJadwal(@PathVariable int id) {
        return ticketService.findTicketByJadwal(id);
    }

    @GetMapping("/ticket-member/{id}")
    public ResponseEntity<?> findTicketByOrderanId(@PathVariable int id) {
        return ticketService.findTicketByOrderanId(id);
    }
}
