package id.co.nexsoft.bioskopbackend.dto.respons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import id.co.nexsoft.bioskopbackend.entities.Seat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
@NoArgsConstructor
@Getter
@Setter

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MemberOrderResponse {

    private int id;

    private String name;

    private LocalDate orderDate;

    private long totalTicket;

    private long totalPrice;

    private String orderStatus;

    private LocalDate showDate;

    private LocalTime showTime;

    private String roomName;

    private String roomType;

    private String username;

    private String email;

    private int ticketPrice;

    public MemberOrderResponse(int id, String name, LocalDate orderDate, long totalTicket, long totalPrice, String orderStatus, LocalDate showDate, LocalTime showTime, String roomName, String roomType) {
        this.id = id;
        this.name = name;
        this.orderDate = orderDate;
        this.totalTicket = totalTicket;
        this.totalPrice = totalPrice;
        this.orderStatus = orderStatus;
        this.showDate = showDate;
        this.showTime = showTime;
        this.roomName = roomName;
        this.roomType = roomType;
    }

    public MemberOrderResponse(int id, String name, LocalDate orderDate, long totalTicket, long totalPrice, String orderStatus, LocalDate showDate, LocalTime showTime, String roomName, String roomType, String username, String email, int ticketPrice) {
        this.id = id;
        this.name = name;
        this.orderDate = orderDate;
        this.totalTicket = totalTicket;
        this.totalPrice = totalPrice;
        this.orderStatus = orderStatus;
        this.showDate = showDate;
        this.showTime = showTime;
        this.roomName = roomName;
        this.roomType = roomType;
        this.username = username;
        this.email = email;
        this.ticketPrice = ticketPrice;
    }
}
