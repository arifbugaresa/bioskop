package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @GetMapping("/room")
    public ResponseEntity<?> findAllRoom() {
        return roomService.findAllRoom();
    }

    @GetMapping("/room-paginated")
    public ResponseEntity<?> findRoomPaginated(
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize
    ) {
        return roomService.findRoomPaginated(pageNo, pageSize);
    }

    @GetMapping("/room/{id}")
    public ResponseEntity<?> findRoomById(@PathVariable int id) {
        return roomService.findRoomById(id);
    }

    @PostMapping("/room")
    public ResponseEntity<?> addRoom(@Valid @RequestBody Room room) {
        return roomService.addRoom(room);
    }

    @PostMapping("/room/{id}")
    public ResponseEntity<?> updateRoom(@PathVariable int id, @Valid @RequestBody Room room) {
        return roomService.updateRoom(id, room);
    }

}
