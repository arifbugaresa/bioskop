package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.ScheduleDTO;
import id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Integer> {
    Schedule findById(int id);
    List<Schedule> findAll();

    // member get schedule paginated
    @Query(value= "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse" +
            "(s.id, m.name, m.description, s.showTime, s.showDate, r.type, r.price, m.dataImage, r.name) " +
            "FROM Schedule s " +
            "JOIN s.movie m " +
            "JOIN s.room r " +
            "WHERE s.showDate >= :today AND m.name LIKE %:keyword% " +
            "ORDER BY s.showDate DESC, s.room.type, s.showTime ASC")
    Page<ScheduleResponse> findSchedulePaginated(Pageable pageable, @Param("today") LocalDate today, @Param("keyword") String keyword);

    // member get schedule today
    @Query(value= "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse" +
            "(s.id, m.name, m.description, s.showTime, s.showDate, r.type, r.price, m.dataImage, r.name) " +
            "FROM Schedule s " +
            "JOIN s.movie m " +
            "JOIN s.room r " +
            "WHERE s.showDate = :today " +
            "ORDER BY s.showDate DESC, s.showTime ASC")
    List<ScheduleResponse> findScheduleToday(@Param("today") LocalDate today);

    //admin get schedule paginated
    @Query("SELECT new id.co.nexsoft.bioskopbackend.entities.Schedule" +
            "(s.id, r, m, s.showTime, s.showDate) " +
            "FROM Schedule s JOIN s.movie m JOIN s.room r " +
            "ORDER BY s.showDate DESC, s.showTime ASC")
    Page<Schedule> schedulePaginatedAdmin(Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO schedule(id, show_date, show_time, id_movie, id_room) " +
            "VALUES(:#{#schedule.id}, :#{#schedule.showDate}, :#{#schedule.showTime}," +
            " :#{#schedule.movieId}, :#{#schedule.roomId})", nativeQuery = true)
    void saveSchedule(@Param("schedule") ScheduleDTO schedule);

    @Query(value = "SELECT COUNT(show_date) FROM schedule WHERE show_date = :today", nativeQuery = true)
    int findMoviesToday(@Param("today") LocalDate today);

    @Query(value = "SELECT COUNT(id) " +
            "FROM schedule " +
            "WHERE show_date = :#{#schedule.showDate} " +
            "AND show_time = :#{#schedule.showTime} " +
            "AND id_room = :#{#schedule.roomId}", nativeQuery = true)
    int findScheduleExist(@Param("schedule") ScheduleDTO schedule);

}
