package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.dto.requests.ScheduleDTO;
import id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse;
import id.co.nexsoft.bioskopbackend.entities.Movie;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import id.co.nexsoft.bioskopbackend.repositories.MovieRepository;
import id.co.nexsoft.bioskopbackend.repositories.RoomRepository;
import id.co.nexsoft.bioskopbackend.repositories.ScheduleRepository;
import id.co.nexsoft.bioskopbackend.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    public ResponseEntity<?> addSchedule(ScheduleDTO scheduleDTO) {

        if (scheduleRepository.findScheduleExist(scheduleDTO) != 0) {
            return ResponseEntity.badRequest()
                    .body("This room has been showing a movie!");
        }

        scheduleRepository.saveSchedule(scheduleDTO);

        List<Schedule> scheduleList = scheduleRepository.findAll();

        return ResponseEntity.ok()
                .body(scheduleList);
    }

    public ResponseEntity<?> findScheduleById(int id) {
        Optional<Schedule> scheduleRepo = Optional.ofNullable(scheduleRepository.findById(id));
        if (!scheduleRepo.isPresent())
            return ResponseEntity.notFound().build();

        Schedule schedule = scheduleRepository.findById(id);
        return ResponseEntity.ok()
                .body(schedule);
    }

    // member get schedules paginated
    public ResponseEntity<?> findSchedulePaginated(int pageNo, int pageSize, String keyword) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        LocalDate today = LocalDate.now();

        Page<ScheduleResponse> scheduleResponses = scheduleRepository.findSchedulePaginated(pageable, today, keyword);

        return ResponseEntity.ok()
                .body(scheduleResponses);
    }

    // member get schedule today
    public ResponseEntity<?> findScheduleToday() {

        LocalDate today = LocalDate.now();

        List<ScheduleResponse> scheduleResponses = scheduleRepository.findScheduleToday(today);

        return ResponseEntity.ok()
                .body(scheduleResponses);
    }


    // admin get schedules paginated
    public ResponseEntity<?> schedulePaginatedAdmin(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<Schedule> scheduleResponses = scheduleRepository.schedulePaginatedAdmin(pageable);

        return ResponseEntity.ok()
                .body(scheduleResponses);
    }
}
