package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.Seat;
import id.co.nexsoft.bioskopbackend.services.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SeatController {

    @Autowired
    private SeatService seatService;

    @GetMapping("/admin/seat")
    public ResponseEntity<?> findAllSeat() {
        return seatService.findAllSeat();
    }

    @PostMapping("/admin/seat")
    public ResponseEntity<?> addSeat(@Valid @RequestBody List<Seat> seats) {
        return seatService.addSeat(seats);
    }

    @GetMapping("/admin/generate-seat")
    public ResponseEntity<?> generateDummySeat() {
        return seatService.generateDummySeat();
    }
}
