package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.dto.requests.LoginRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.RegisterRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.UserEditRequest;
import id.co.nexsoft.bioskopbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/member/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest registerRequest) {
        return userService.register(registerRequest);
    }

    @PostMapping("/user/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest){
        return userService.login(loginRequest);
    }

    @PostMapping("/admin/register")
    public ResponseEntity<?> registerAdmin(@Valid @RequestBody RegisterRequest registerRequest) {
        return userService.registerAdmin(registerRequest);
    }

    @PostMapping("/admin/reset-password-member/{id}")
    public ResponseEntity<?> resetPasswordMember(@PathVariable int id) {
        return userService.resetPasswordMember(id);
    }

    // admin get all user pagable
    @GetMapping("/admin/users-paginated")
    public ResponseEntity<?> getUserPaginated(
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize,
            @RequestParam(required = false) String keyword
            ) {
        return userService.getUserPaginated(pageNo, pageSize, keyword);
    }

    @PostMapping("/user/update-profile")
    public ResponseEntity<?> updateProfileUser(@Valid @RequestBody UserEditRequest userEditRequest) {
        return userService.updateProfile(userEditRequest);
    }

    @PostMapping("/admin/update-member")
    public ResponseEntity<?> adminUpdateMember(@Valid @RequestBody UserEditRequest userEditRequest) {
        return userService.adminUpdateMember(userEditRequest);
    }
}