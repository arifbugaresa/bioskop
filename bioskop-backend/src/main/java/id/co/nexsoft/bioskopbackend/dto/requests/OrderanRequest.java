package id.co.nexsoft.bioskopbackend.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class OrderanRequest {

    // ticket
    @NotNull
    private int idSchedule;

    // ticket
    @NotNull
    private List<String> listSeat;

    // ticket
    private String status;

    // orderan
    @NotNull
    private int idUser;

    // orderan
    private String orderStatus;

    // orderan
    private LocalDate orderDate;

    // orderan detail
    private int idTicket;

}
