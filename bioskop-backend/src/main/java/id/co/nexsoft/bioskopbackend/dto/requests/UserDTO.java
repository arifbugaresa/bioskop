package id.co.nexsoft.bioskopbackend.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class UserDTO {

    private int id;

    private String name;

    private String username;

    private String address;

    private String phone_number;

    private String email;
}
