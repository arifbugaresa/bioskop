package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.OrderanRequest;
import id.co.nexsoft.bioskopbackend.dto.respons.MemberOrderResponse;
import id.co.nexsoft.bioskopbackend.dto.respons.OrderanDetailResponse;
import id.co.nexsoft.bioskopbackend.entities.Orderan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderanRepository extends CrudRepository<Orderan, Integer> {

    @Modifying
    @Query(value = "INSERT INTO orderan(id_user, order_status, order_date) " +
            "VALUES(:#{#order.idUser}, :#{#order.orderStatus}, :#{#order.orderDate})", nativeQuery = true)
    void checkoutOrderan(@Param("order") OrderanRequest order);

    Orderan findTopByOrderByIdDesc();

    Orderan findById(int id);

    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.MemberOrderResponse" +
        "(o.id, m.name , o.orderDate, count(o.id), sum(r.price), o.orderStatus, s.showDate, s.showTime, r.name, r.type, u.username, u.email, r.price) " +
        "FROM Orderan o " +
        "JOIN o.user u " +
        "JOIN o.orderanDetail od " +
        "JOIN od.ticket t " +
        "JOIN t.schedule s " +
        "JOIN s.room r " +
        "JOIN s.movie m " +
        "WHERE o.orderStatus = :status " +
        "GROUP BY o.id " +
            "ORDER BY o.id DESC")
    Page<MemberOrderResponse> findOrderanPaginated(Pageable pageable, @Param("status") String status);

    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.MemberOrderResponse" +
            "(o.id, m.name , o.orderDate, count(o.id), sum(r.price), o.orderStatus, s.showDate, s.showTime, r.name, r.type) " +
            "FROM Orderan o " +
            "JOIN o.user u " +
            "JOIN o.orderanDetail od " +
            "JOIN od.ticket t " +
            "JOIN t.schedule s " +
            "JOIN s.room r " +
            "JOIN s.movie m " +
            "WHERE o.user.id = :id " +
            "GROUP BY o.id " +
            "ORDER BY o.id DESC")
    Page<MemberOrderResponse> findMyOrders(Pageable pageable, @Param("id") int id);

    @Modifying
    @Query(value = "UPDATE orderan o " +
            "SET o.order_status = :status WHERE o.id = :id", nativeQuery = true)
    void confirmOrderan(@Param("id") int id, @Param("status") String status);

    // orderan detail
    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.OrderanDetailResponse" +
            "(o.id, u.name, u.email, u.phone_number, o.orderDate) " +
            "FROM Orderan o " +
            "JOIN o.user u " +
            "WHERE o.id = :id")
    OrderanDetailResponse findOrderanDetail(@Param("id") int id);

    @Query(value ="SELECT id_seat " +
            "FROM orderan_detail od " +
            "JOIN orderan o " +
            "ON o.id = od.orderan_id " +
            "JOIN ticket t " +
            "ON od.id_ticket = t.id " +
            "WHERE o.id = :id", nativeQuery = true)
    List<String> findSeatOrderDetail(@Param("id") int id);

    @Query( value= "SELECT sum(price) " +
            "FROM orderan_detail od " +
            "JOIN orderan o " +
            "ON o.id = od.orderan_id " +
            "JOIN ticket t " +
            "ON od.id_ticket = t.id " +
            "JOIN schedule sc " +
            "on t.id_schedule = sc.id " +
            "JOIN room r " +
            "ON r.id = sc.id_room " +
            "WHERE o.id = :id" , nativeQuery = true)
    int findTotalPriceOrder(@Param("id") int id);

    @Modifying
    @Query( value="DELETE FROM orderan WHERE id = :id", nativeQuery = true)
    void deleteOrderan(@Param("id") int id);
}
