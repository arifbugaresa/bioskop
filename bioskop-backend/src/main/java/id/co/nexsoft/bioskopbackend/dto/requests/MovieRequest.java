package id.co.nexsoft.bioskopbackend.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Lob;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class MovieRequest {

    private int id;

    private String name;

    @Lob
    private String description;

    private int duration;

    @Lob
    private byte[] dataImage;
}
