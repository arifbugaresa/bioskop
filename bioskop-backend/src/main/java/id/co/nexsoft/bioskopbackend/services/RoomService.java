package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public ResponseEntity<?> addRoom(Room room) {

        if(roomRepository.findByName(room.getName()) != null )  {
            return ResponseEntity.badRequest()
                    .body("This room already exist!");
        }

        roomRepository.saveRoom(room);
        List<Room> listRoom = roomRepository.findAllRoom();
        return ResponseEntity.ok()
                .body(listRoom);
    }

    public ResponseEntity<?> findRoomById(int id) {
        Room room = roomRepository.findRoom(id);
        return ResponseEntity.ok()
                .body(room);
    }

    public ResponseEntity<?> findAllRoom() {
        List<Room> rooms = roomRepository.findAllRoom();
        return ResponseEntity.ok()
                .body(rooms);
    }

    public ResponseEntity<?> findRoomPaginated(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<Room> rooms = roomRepository.findRoomPaginated(pageable);
        return ResponseEntity.ok()
                .body(rooms);
    }

    public ResponseEntity<?> updateRoom(int id, Room room) {
        Optional<Room> roomRepo = Optional.ofNullable(roomRepository.findRoom(id));
        if (!roomRepo.isPresent())
            return ResponseEntity.notFound().build();

        roomRepository.updateRoom(id, room);

        List<Room> listRoom = roomRepository.findAllRoom();

        return ResponseEntity.ok()
                .body(listRoom);
    }

}
