package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.dto.requests.MovieRequest;
import id.co.nexsoft.bioskopbackend.entities.Movie;
import id.co.nexsoft.bioskopbackend.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movie")
    public ResponseEntity<?> findAllMovie() {
        return movieService.findAllMovie();
    }

    @GetMapping("/movie-paginated")
    public ResponseEntity<?> findMoviePaginated(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize
    ) {
        return movieService.findMoviePaginated(pageNo, pageSize);
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity<?> findMovieById(@PathVariable int id) {
        return movieService.findMovie(id);
    }

    @PostMapping("/movie")
    public ResponseEntity<?> addMovie(@Valid @RequestBody MovieRequest movieRequest) {
       return movieService.addMovie(movieRequest);
    }

    @PostMapping("/movie/{id}")
    public ResponseEntity<?> updateMovie(@PathVariable int id, @Valid @RequestBody Movie movie) {
        return movieService.updateMovie(id, movie);
    }
}
