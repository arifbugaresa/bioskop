package id.co.nexsoft.bioskopbackend.dto.respons;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter

public class OrderanDetailResponse {

    private int id;

    private String name;

    private String email;

    private String phone_number;

    private int ticket;

    private int totalPrice;

    private String seats;

    private LocalDate orderDate;

    private String status;

    public OrderanDetailResponse(int id, String name, String email, String phone_number, LocalDate orderDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.orderDate = orderDate;
    }
}
