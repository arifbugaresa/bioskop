package id.co.nexsoft.bioskopbackend.dto.respons;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class DashboardResponse {

    private int movies;

    private int tickets;

    private int incomes;
}
