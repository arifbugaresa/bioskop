package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.WebSecurityConfig;
import id.co.nexsoft.bioskopbackend.dto.requests.LoginRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.RegisterRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.UserEditRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.UserDTO;
import id.co.nexsoft.bioskopbackend.dto.respons.LoginResponse;
import id.co.nexsoft.bioskopbackend.dto.respons.UserResponse;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.User;
import id.co.nexsoft.bioskopbackend.repositories.UserRepository;
import id.co.nexsoft.bioskopbackend.security.jwt.JwtUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final MyUserDetailService userDetailService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    private ModelMapper modelMapper = new ModelMapper();

    public UserService(AuthenticationManager authenticationManager, JwtUtil jwtUtil, MyUserDetailService userDetailService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailService = userDetailService;
    }

    public ResponseEntity<?> login(LoginRequest loginRequest){
        User userRepo = userRepository.findByUsername(loginRequest.getUsername());
        if (userRepo == null ) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Username not found!");
        }

        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                                    loginRequest.getUsername(),
                                    loginRequest.getPassword()
                            )
                    );

            UserDetails userDetails = userDetailService.loadUserByUsername(loginRequest.getUsername());

            // membuat token sesuai user
            final String jwt = jwtUtil.generateToken(userDetails);

            User user = userRepository.findByUsername(loginRequest.getUsername());
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setUser(userDTO);
            loginResponse.setToken(jwt);
            loginResponse.setRole(userDetails.getAuthorities().toArray()[0].toString());
            return ResponseEntity.ok()
                    .body(loginResponse);

        } catch(BadCredentialsException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("incorrect password!");
        }
    }

    public ResponseEntity<?> register(RegisterRequest registerRequest) {

        if( userRepository.existsByUsername(registerRequest.getUsername()) ) {
            return ResponseEntity
                    .badRequest()
                    .body("Username already taken!");
        }

        if( userRepository.existsByEmail(registerRequest.getEmail()) ) {
            return ResponseEntity
                    .badRequest()
                    .body("Email is already in use!");
        }

        @Valid
        User user = modelMapper.map(registerRequest, User.class);
        user.setPassword(webSecurityConfig.passwordEncoder().encode(registerRequest.getPassword()));
        user.setCreated_at(LocalDate.now());
        user.setRole("ROLE_MEMBER");

        userRepository.save(user);

        return ResponseEntity.ok()
                .body("Register success");
    }

    public ResponseEntity<?> registerAdmin(RegisterRequest registerRequest) {

        if( userRepository.existsByUsername(registerRequest.getUsername()) ) {
            return ResponseEntity
                    .badRequest()
                    .body("Username already taken!");
        }

        if( userRepository.existsByEmail(registerRequest.getEmail()) ) {
            return ResponseEntity
                    .badRequest()
                    .body("Email is already in use!");
        }

        User user = modelMapper.map(registerRequest, User.class);
        user.setPassword(webSecurityConfig.passwordEncoder().encode(registerRequest.getPassword()));
        user.setCreated_at(LocalDate.now());
        user.setRole("ROLE_ADMIN");

        userRepository.save(user);

        return ResponseEntity.ok()
                .body("Register success");

    }

    public ResponseEntity<?> getUserPaginated(int pageNo, int pageSize, String keyword) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<UserResponse> userPaginated = (keyword == null) ? userRepository.getUserPaginated(pageable)
                                            : userRepository.searchUserPaginated(keyword, pageable);

        return ResponseEntity.ok()
                .body(userPaginated);
    }

    public ResponseEntity<?> updateProfile(UserEditRequest userEditRequest) {
        Optional<User> userRepo = Optional.ofNullable( userRepository.findById(userEditRequest.getId()));

        if (!userRepo.isPresent())
            return ResponseEntity.notFound().build();

        userRepository.update(userEditRequest);
        UserDTO userUpdate = userRepository.findUser(userEditRequest.getId());
        return ResponseEntity.ok()
                .body(userUpdate);
    }

    public ResponseEntity<?> adminUpdateMember(UserEditRequest userEditRequest) {
        Optional<User> userRepo = Optional.ofNullable( userRepository.findById(userEditRequest.getId()));

        if (!userRepo.isPresent())
            return ResponseEntity.notFound().build();

        userRepository.update(userEditRequest);
        List<UserResponse> listUser = userRepository.findAllUser();
        return ResponseEntity.ok()
                .body(listUser);
    }

    public ResponseEntity<?> resetPasswordMember(int id) {
        Optional<User> userRepo = Optional.ofNullable( userRepository.findById(id) );

        if (!userRepo.isPresent())
            return ResponseEntity.notFound().build();

        User user = userRepository.findById(id);

        //pengecekan memastikan yang akan diubah adalah member
        if(user.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
            return ResponseEntity
                    .badRequest()
                    .body("Cannot Reset Admin Password!");
        }

        String defaultPassword = "123123";
        user.setPassword(webSecurityConfig.passwordEncoder().encode(defaultPassword));
        userRepository.save(user);

        return ResponseEntity.ok()
                .body(user);
    }

//    public User findByUsername(String username) {
//        return userRepository.findByUsername(username);
//    }
//
//    public User findUserById(int id) {
//        return userRepository.findById(id);
//    }

}
