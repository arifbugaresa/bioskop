package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.dto.requests.ScheduleDTO;
import id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import id.co.nexsoft.bioskopbackend.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/admin/schedule")
    public ResponseEntity<?> addSchedule(@Valid @RequestBody ScheduleDTO scheduleDTO) {
        return scheduleService.addSchedule(scheduleDTO);
    }

    @GetMapping("/admin/schedule/{id}")
    public ResponseEntity<?> findScheduleById(@PathVariable int id) {
        return scheduleService.findScheduleById(id);
    }

    @GetMapping("/admin/schedule-paginated")
    public ResponseEntity<?> SchedulePaginatedAdmin(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize
    ) {
        return scheduleService.schedulePaginatedAdmin(pageNo, pageSize);
    }

    @GetMapping("/member/schedule-paginated")
    public ResponseEntity<?> findSchedulePaginated(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "8") Integer pageSize,
            @RequestParam(defaultValue = "") String keyword
    ) {
        return scheduleService.findSchedulePaginated(pageNo, pageSize, keyword);
    }

    @GetMapping("/member/schedule-today")
    public ResponseEntity<?> findScheduleToday() {
        return scheduleService.findScheduleToday();
    }

}
