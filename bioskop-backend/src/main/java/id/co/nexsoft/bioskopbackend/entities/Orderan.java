package id.co.nexsoft.bioskopbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class Orderan {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private LocalDate orderDate;

    private String orderStatus;

    @OneToMany(targetEntity = OrderanDetail.class, mappedBy = "orderan", cascade = CascadeType.ALL)
    private List<OrderanDetail> orderanDetail;


}
