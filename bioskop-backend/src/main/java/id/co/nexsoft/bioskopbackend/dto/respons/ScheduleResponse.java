package id.co.nexsoft.bioskopbackend.dto.respons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class ScheduleResponse {

    private int id;

    private String name;

    private String description;

    private LocalTime showTime;

    private LocalDate showDate;

    private String type;

    private int price;

    private byte[] dataImage;

    private String roomName;
}