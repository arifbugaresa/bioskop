package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.MovieRequest;
import id.co.nexsoft.bioskopbackend.dto.respons.MovieDto;
import id.co.nexsoft.bioskopbackend.dto.respons.ScheduleResponse;
import id.co.nexsoft.bioskopbackend.entities.Movie;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer> {

    @Query(value= "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.MovieDto" +
            "(m.id, m.name, m.description, m.duration, m.dataImage) " +
            "FROM Movie m " )
    List<MovieDto> findAllMovie();

    //admin get movie paginated
    @Query("SELECT new id.co.nexsoft.bioskopbackend.dto.respons.MovieDto" +
            "(m.id, m.name, m.description, m.duration, m.dataImage) " +
            "FROM Movie m " +
            "ORDER BY m.id DESC")
    Page<MovieDto> findMoviePaginated(Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.MovieDto" +
            "(m.id, m.name, m.description, m.duration, m.dataImage) " +
            "FROM Movie m " +
            "WHERE m.id = :id")
    MovieDto findMovie(@Param("id") int id);

    Movie findById(int id);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO movie (id, description,duration,name,data_image) " +
            "VALUES (:#{#movie.id}, :#{#movie.description}, :#{#movie.duration}, " +
            ":#{#movie.name}, :#{#movie.dataImage})", nativeQuery = true)
    void saveMovie(@Param("movie") MovieRequest movieRequest);

    @Transactional
    @Modifying
    @Query(value = "UPDATE movie m " +
            "SET m.name = :#{#movie.name}, " +
            "m.duration = :#{#movie.duration}, " +
            "m.data_image = :#{#movie.dataImage}, " +
            "m.description = :#{#movie.description} " +
            "WHERE m.id = :id", nativeQuery = true)
    void updateMovie(@Param("id") int id, @Param("movie") Movie movie);

}
