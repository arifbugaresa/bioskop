package id.co.nexsoft.bioskopbackend.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class RegisterRequest {

    @NotBlank
    @NotNull
    private String name;

    @NotBlank
    @NotNull
    private String username;

    @NotBlank
    @NotNull
    @Pattern(
            regexp =  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$",
            message = "Minimum 6 Character, Uppercase, Lowercase, Number"
    )
    private String password;

    @NotBlank
    @NotNull
    private String address;

    @NotBlank
    @NotNull
    @Pattern(
            regexp = "^(?=.\\d.).{9,12}$",
            message = "Must be number 9-12 Digit"
    )
    private String phone_number;

    @NotBlank
    @NotNull
    @Email
    private String email;
}
