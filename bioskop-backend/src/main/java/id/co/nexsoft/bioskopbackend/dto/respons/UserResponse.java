package id.co.nexsoft.bioskopbackend.dto.respons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserResponse {

    private int id;

    private String name;

    private String username;

    private String address;

    private String phone_number;

    private String email;

    private LocalDate created_at;

    private LocalDate deleted_at;

    private String role;

}
