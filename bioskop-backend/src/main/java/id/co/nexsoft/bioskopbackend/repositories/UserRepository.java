package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.UserDTO;
import id.co.nexsoft.bioskopbackend.dto.requests.UserEditRequest;
import id.co.nexsoft.bioskopbackend.dto.respons.UserResponse;
import id.co.nexsoft.bioskopbackend.entities.Room;
import id.co.nexsoft.bioskopbackend.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findById(int id);
    User findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    @Query( value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.UserResponse " +
            "(u.id, u.name, u.username, u.address, u.phone_number, u.email, u.created_at, u.deleted_at, u.role)  " +
            "FROM User u ORDER BY u.role ASC, u.name ASC")
    List<UserResponse> findAllUser();

    @Query( value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.UserResponse " +
            "(u.id, u.name, u.username, u.address, u.phone_number, u.email, u.created_at, u.deleted_at, u.role)  " +
            "FROM User u ORDER BY u.role ASC, u.name ASC")
    Page<UserResponse> getUserPaginated(Pageable pageable);

    @Query( value = "SELECT new id.co.nexsoft.bioskopbackend.dto.respons.UserResponse " +
            "(u.id, u.name, u.username, u.address, u.phone_number, u.email, u.created_at, u.deleted_at, u.role)  " +
            "FROM User u WHERE u.username LIKE %:keyword% ORDER BY u.role ASC, u.name ASC")
    Page<UserResponse> searchUserPaginated(@Param("keyword") String keyword, Pageable pageable);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value ="UPDATE user u " +
            "SET u.name = :#{#user.name}, " +
            "u.username = :#{#user.username}, " +
            "u.address = :#{#user.address} " +
            "WHERE u.id = :#{#user.id}", nativeQuery = true)
    void update(@Param("user") UserEditRequest user);

    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.dto.requests.UserDTO" +
            "(u.id, u.name, u.username, u.address, u.phone_number, u.email ) " +
            "FROM User u WHERE u.id = :id")
    UserDTO findUser(@Param("id") int id);

    @Query(value = "SELECT email FROM user u WHERE u.id = :id", nativeQuery = true)
    String findEmailById(@Param("id") int id);

}
