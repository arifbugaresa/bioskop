import React, { useEffect, useState } from 'react';
import '../../../assets/css/contentRoomAdmin.css'
import { Link } from 'react-router-dom';
import { Col, Container, Row, Table } from 'reactstrap';
import { useSelector } from 'react-redux';
import FormAddRoom from '../form/FormAddRoom';
import FormEditRoom from '../form/FormEditRoom';
import { getData } from '../../../service/Fetch';
import CurrencyFormat from 'react-currency-format';


const ContentRoomAdmin = () => {

    const [rooms, setRooms] = useState([]);

    const [response, setResponse] = useState();

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    const [page, setPage] = useState(0);

    const token = useSelector(state => state.auth.token);

    useEffect(() => {
        const url = `admin/room-paginated?pageNo=${page}`

        getData(url, token)
            .then(res => {
                setRooms(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page])

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>Room Management</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Room</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <FormAddRoom setResponse={setResponse} />
                </Col>
            </Row>

            {/* Table Room */}
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th className="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                rooms.length !== 0 ?
                                    rooms.map((room) =>
                                        <tr key={room.id}>
                                            <td>#</td>
                                            <td>{room.name}</td>
                                            <td>{room.type}</td>
                                            <CurrencyFormat
                                                value={room.price}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"Rp. "}
                                                renderText={(value) => (
                                                    <td>{value}</td>
                                                )}
                                            />
                                            <td className="text-center">
                                                <FormEditRoom room={room} setResponse={setResponse} />
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="7" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>

            {/* Pagination */}
            <Row>
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page- 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentRoomAdmin;