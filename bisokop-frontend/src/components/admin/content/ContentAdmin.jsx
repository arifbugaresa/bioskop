import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import ContentDashboardAdmin from './ContentDashboardAdmin';
import ContentRoomAdmin from './ContentRoomAdmin';
import ContentScheduleAdmin from './ContentScheduleAdmin';
import ContentUserAdmin from './ContentUserAdmin';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import swal from 'sweetalert';
import jwtDecode from 'jwt-decode';
import { authAction } from '../../../redux/reducer/AuthReducer';
import ContentMovieAdmin from './ContentMovieAdmin';
import ContentSeatRegular from './ContentSeatRegular';
import ContentSeatVip from './ContentSeatVip';
import ContentAccountAdmin from './ContentAccountAdmin';
import ContentOrderAdmin from './ContentOrderAdmin';
import ContentOrderRequest from './ContentOrderRequest';

const ContentAdmin = () => {

    let {path} = useRouteMatch();

    const token = useSelector((state) => state.auth.token);

    const dispatch = useDispatch();

    if (token) {
        const decoded = jwtDecode(token);
        if (decoded.exp < Date.now() / 1000) {
            swal("Error!", "Your session expired", "error");
            dispatch(authAction.logout());
        }
    }

    return (
        <Switch>
            <Route exact path={`${path}`} component={ContentDashboardAdmin} />
            <Route exact path={`${path}/room`} component={ContentRoomAdmin} />
            <Route exact path={`${path}/movie`} component={ContentMovieAdmin} />
            <Route exact path={`${path}/schedule`} component={ContentScheduleAdmin} />
            <Route exact path={`${path}/user`} component={ContentUserAdmin} />
            <Route exact path={`${path}/account`} component={ContentAccountAdmin} />
            <Route exact path={`${path}/order`} component={ContentOrderAdmin} />
            <Route exact path={`${path}/order-request`} component={ContentOrderRequest} />
            <Route exact path={`${path}/seat-regular/:id`} component={ContentSeatRegular} />
            <Route exact path={`${path}/seat-vip/:id`} component={ContentSeatVip} />
        </Switch>
    );
}

export default ContentAdmin;