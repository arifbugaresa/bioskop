import React, { useEffect, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import { Col, Button, Container, Row, Table } from 'reactstrap';
import { getData } from '../../../service/Fetch';
import FormAddSchedule from '../form/FormAddSchedule';

const ContentScheduleAdmin = () => {

    const [schedules, setSchedules] = useState([]);

    const token = useSelector(state => state.auth.token);

    const [response, setResponse] = useState();

    const history = useHistory()

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    const [page, setPage] = useState(0);

    useEffect(() => {
        const url = `admin/schedule-paginated?pageNo=${page}`

        getData(url, token)
            .then(res => {
                setSchedules(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page])

    const checkSeatHandler = (schedule) => {
        // cek regular atau vip
        schedule.room.type === "VIP" ?
            history.push(`seat-vip/${schedule.id}`, { data: schedule }) :
            history.push(`seat-regular/${schedule.id}`, { data: schedule })
    }

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>Schedule Management</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Schedule</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <FormAddSchedule setResponse={setResponse} />
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Movie</th>
                                <th>Room</th>
                                <th>Price</th>
                                <th>Time</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                schedules.length !== 0 ?
                                    schedules.map(schedule =>
                                        <tr key={schedule.id}>
                                            <td>#</td>
                                            <td>{schedule.movie.name}</td>
                                            <td>{schedule.room.name}</td>
                                            <CurrencyFormat
                                                value={schedule.room.price}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"Rp. "}
                                                renderText={(value) => (
                                                    <td>{value}</td>
                                                )}
                                            />
                                            <td>{schedule.showTime}</td>
                                            <td>{schedule.showDate}</td>
                                            <td className="text-center">
                                                <Button
                                                    onClick={() => checkSeatHandler(schedule)}
                                                    color="warning"
                                                    className="text-white"
                                                    size="sm">
                                                    Seat
                                                </Button>{' '}
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="7" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {/* Pagination */}
            <Row>
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page - 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentScheduleAdmin;