import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getData, postData } from '../../../service/Fetch';
import {
    Col,
    Container,
    Row,
    Table,
    Button
} from 'reactstrap';
import swal from 'sweetalert';
import '../../../assets/css/contentOrderAdmin.css'
import ModalOrderDetail from '../form/ModalOrderDetail';
import { transactionAction } from '../../../redux/reducer/TransactionReducer';

const ContentOrderRequest = () => {

    const [orderRequest, setOrderRequest] = useState();

    const token = useSelector(state => state.auth.token);

    const [page, setPage] = useState(0);

    const [response, setResponse] = useState();

    // approve
    const dispatch = useDispatch();

    const processApprove = useSelector((state) => state.transaction.processApprove);

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    useEffect(() => {
        const url = `admin/orderan?pageNo=${page}&pageSize=5`

        getData(url, token)
            .then(res => {
                setOrderRequest(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page])

    const approveHandler = (order) => {

        swal({
            title: "Are you sure?",
            text: "Are you sure you want to accept this order?",
            icon: "info",
            buttons: true,
        })
            .then((willConfirm) => {
                if (willConfirm) {
                    dispatch(transactionAction.approve(true));
                    postData(`admin/confirm-orderan?id=${order.id}`, {}, token)
                        .then(res => {
                            setResponse(res.data)
                        })
                        .then(() => {
                            dispatch(transactionAction.approve(false));
                            swal({
                                title: "Success!",
                                text: "Success accept this order",
                                icon: "success",
                            })
                        })
                        .catch(err => {
                            dispatch(transactionAction.approve(false));
                            swal("Error", err.response.data, "error");
                        });
                }
            });
    }

    const rejectHandler = (order) => {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to reject this order?",
            icon: "info",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    postData(`admin/reject-orderan?id=${order.id}`, {}, token)
                        .then(res => {
                            setResponse(res.data)
                        })
                        .then(() => {
                            swal({
                                title: "Success!",
                                text: "Success reject this order",
                                icon: "success",
                            });
                        })
                        .catch(err => {
                            swal("Error", err.response.data, "error");
                        });
                }
            });
    }


    return (
        <Container className="pt-2">
            {
                !processApprove ?
                    <>
                        <Row className="mt-4 mb-4">
                            <Col>
                                <h5>Order Request</h5>
                            </Col>
                            <Col className="flex-column align-items-end d-none d-md-flex">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">Order</li>
                                    </ol>
                                </nav>
                            </Col>
                        </Row>
                        <Row className="mt-4 mb-4">
                            <Col>
                                <Table striped bordered hover className="text-center">
                                    <thead>
                                        <tr>
                                            <th>OrderID</th>
                                            <th>Order Date</th>
                                            <th>Ticket</th>
                                            <th>Movie</th>
                                            <th>Show Time</th>
                                            <th>Show Date</th>
                                            <th>Status</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            orderRequest !== undefined ?
                                                orderRequest.length !== 0 ?
                                                    orderRequest.map(or =>
                                                        <tr key={or.id}>
                                                            <td>{or.id}</td>
                                                            <td>{or.orderDate}</td>
                                                            <td>{or.totalTicket} Ticket</td>
                                                            <td>{or.name}</td>
                                                            <td>{or.showTime}</td>
                                                            <td>{or.showDate}</td>
                                                            <td>
                                                                <p
                                                                    size="sm"
                                                                    className={
                                                                        or.orderStatus === 'requested' ?
                                                                            "order-requested" : "order-reserved"
                                                                    }
                                                                    disabled="true">
                                                                    {or.orderStatus}
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <Button
                                                                    onClick={() => approveHandler(or)}
                                                                    className="btn-approve">
                                                                    <i class="bi bi-check2-square"></i>
                                                                </Button>
                                                                <Button
                                                                    onClick={() => rejectHandler(or)}
                                                                    className="btn-reject">
                                                                    <i class="bi bi-x-square"></i>
                                                                </Button>
                                                                <ModalOrderDetail or={or} />
                                                            </td>
                                                        </tr>
                                                    ) :
                                                    <tr>
                                                        <td colSpan="8" className="text-center">Data Kosong</td>
                                                    </tr>
                                                :
                                                <p>Fetching data ...</p>
                                        }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>

                        {/* Pagination */}
                        <Row className="mt-4 mb-5">
                            <Col className="d-flex justify-content-center">
                                <nav>
                                    {
                                        paginated.empty ?
                                            <p></p> :
                                            <ul className="pagination">
                                                {
                                                    paginated.first ?
                                                        <li className="page-item disabled">
                                                            <span className="page-link">Previous</span>
                                                        </li> :
                                                        <li
                                                            onClick={() => setPage(paginated.number - 1)}
                                                            className="page-item">
                                                            <span className="page-link">Previous</span>
                                                        </li>
                                                }
                                                {
                                                    Array.from(Array(paginated.totalPages), (event, index) => {
                                                        return (
                                                            <li
                                                                onClick={() => setPage(index)}
                                                                className={index === page ? "page-item active" : "page-item"}>
                                                                <span className="page-link">
                                                                    {index + 1}
                                                                </span>
                                                            </li>
                                                        );
                                                    })
                                                }
                                                {
                                                    paginated.last ?
                                                        <li className="page-item disabled">
                                                            <span className="page-link">Next</span>
                                                        </li> :
                                                        <li
                                                            onClick={() => setPage(paginated.number + 1)}
                                                            className="page-item">
                                                            <span className="page-link">Next</span>
                                                        </li>
                                                }
                                            </ul>
                                    }
                                </nav>
                            </Col>
                        </Row>
                    </> :
                    <>
                        <Row className="mt-4 mb-4">
                            <Col>
                                <h5>Process Approve</h5>
                            </Col>
                            <Col className="flex-column align-items-end d-none d-md-flex">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><i class="bi bi-house"></i></li>
                                        <li className="breadcrumb-item"><Link to="/admin">Dashboard</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">Order</li>
                                    </ol>
                                </nav>
                            </Col>
                        </Row>
                        <div className="d-flex justify-content-center mb-5">
                            <div className="text-center mb-5">
                                <div className="spinner-border mb-3 mt-5" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                                <div className="mb-5">
                                    <h5>Loading...</h5>
                                </div>
                            </div>
                        </div>
                    </>
            }
        </Container>
    );
}

export default ContentOrderRequest;