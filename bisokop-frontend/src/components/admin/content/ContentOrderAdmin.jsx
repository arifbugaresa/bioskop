import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getData } from '../../../service/Fetch';
import {
    Col,
    Container,
    Row,
    Table,
    Button
} from 'reactstrap';
import '../../../assets/css/contentOrderAdmin.css'
import ModalOrderDetail from '../form/ModalOrderDetail';

const ContentOrderAdmin = () => {

    const [orders, setOrders] = useState();

    const token = useSelector(state => state.auth.token);

    const [page, setPage] = useState(0);

    const [response] = useState();

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    useEffect(() => {
        const url = `admin/orderan-accepted?pageNo=${page}&pageSize=5`

        getData(url, token)
            .then(res => {
                setOrders(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page])

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>Order Report</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Order</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>OrderID</th>
                                <th>Order Date</th>
                                <th>Ticket</th>
                                <th>Movie</th>
                                <th>Time</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th className="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orders !== undefined ?
                                    orders.length !== 0 ?
                                        orders.map(or =>
                                            <tr key={or.id}>
                                                <td>{or.id}</td>
                                                <td>{or.orderDate}</td>
                                                <td>{or.totalTicket} Ticket</td>
                                                <td>{or.name}</td>
                                                <td>{or.showTime}</td>
                                                <td>{or.showDate}</td>
                                                <td>
                                                    <p
                                                        size="sm"
                                                        className={
                                                            or.orderStatus === 'requested' ?
                                                                "order-requested" : "order-accepted"
                                                        }
                                                        disabled="true">
                                                        {or.orderStatus}
                                                    </p>
                                                </td>
                                                <td>
                                                    <ModalOrderDetail or={or} />
                                                </td>
                                            </tr>
                                        ) :
                                        <tr>
                                            <td colSpan="8" className="text-center">Data Kosong</td>
                                        </tr>
                                    :
                                    <p>Fetching data ...</p>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {/* Pagination */}
            <Row className="mt-4 mb-5">
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(paginated.number - 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(paginated.number + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentOrderAdmin;