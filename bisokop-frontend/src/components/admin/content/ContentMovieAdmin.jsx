import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col, Container, Row, Table } from 'reactstrap';
import { getData } from '../../../service/Fetch';
import FormAddMovie from '../form/FormAddMovies';
import FormEditMovie from '../form/FormEditMovie';
import '../../../assets/css/contentMovieAdmin.css'
import ModalDetailMovie from '../form/ModalDetailMovie';

const ContentMovieAdmin = () => {

    const [movies, setMovies] = useState([])

    const token = useSelector(state => state.auth.token);

    const [page, setPage] = useState(0);

    const [response, setResponse] = useState();

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    useEffect(() => {
        const url = `admin/movie-paginated?pageNo=${page}&pageSize=5`

        getData(url, token)
            .then(res => {
                setMovies(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page])

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>Movie Management</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Movie</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <FormAddMovie setResponse={setResponse} />
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center align-middle">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Duration</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                movies.length !== 0 ?
                                    movies.map(movie =>
                                        <tr key={movie.id}>
                                            <td>#</td>
                                            <td>
                                                <img
                                                    src={`data:image/*;base64, ` + movie.dataImage}
                                                    className="image-movie" alt="..." />
                                            </td>
                                            <td>{movie.name}</td>
                                            <td>{movie.duration} Hours</td>
                                            <td>
                                                <div className="d-flex justify-content-center">
                                                    <FormEditMovie movie={movie} setResponse={setResponse} />
                                                    <ModalDetailMovie movie={movie} />
                                                </div>
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="6" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>

            {/* Pagination */}
            <Row>
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page - 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentMovieAdmin;