import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import { 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    Form, 
    FormGroup, 
    Label, 
    Input 
} from 'reactstrap';
import { postData } from '../../../service/Fetch';


const FormEditMovie = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => {
        setModal(!modal);
    }

    const token = useSelector(state => state.auth.token);

    const [newMovie, setNewMovie] = useState({
        id: props.movie.id,
        name: props.movie.name,
        description: props.movie.description,
        dataImage: props.movie.dataImage
    });

    const submitHandler = (event) => {

        event.preventDefault();

        postData(`admin/movie/${props.movie.id}`, newMovie, token)
            .then(res => {
                props.setResponse(res.data)
            })
            .then(
                swal({
                    title: "Success!",
                    text: "Success Edit Movie",
                    icon: "success",
                })
            )
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button size="sm" color="warning" className="text-white action-movie" onClick={toggle}><i class="bi bi-pencil-square"></i></Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Edit Movie</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="name" className="mb-2">Title</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setNewMovie({ ...newMovie, name: e.target.value })}
                                value={newMovie.name}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description" className="mb-2">Description</Label>
                            <Input type="textarea" name="description" id="description"
                                className="mb-2" style={{ height: 300 }}
                                onChange={(e) => setNewMovie({ ...newMovie, description: e.target.value })}
                                value={newMovie.description}
                                required />
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default FormEditMovie;