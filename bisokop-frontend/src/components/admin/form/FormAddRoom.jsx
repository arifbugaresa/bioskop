import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { postData } from '../../../service/Fetch';

const FormAddRoom = (props) => {
    const [modal, setModal] = useState(false);
    const token = useSelector(state => state.auth.token);
    const toggle = () => setModal(!modal);
    const [room, setRoom] = useState({
        name: '',
        type: '',
        price: '',
    })

    const submitHandler = (event) => {

        event.preventDefault();

        postData("admin/room", room, token)
            .then(res => {
                props.setResponse(res.data)
                swal({
                    title: "Success!",
                    text: "Success Add New Room",
                    icon: "success",
                });
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button color="success" onClick={toggle} size="sm">Add Room</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Add Room</ModalHeader>
                <ModalBody>
                    <form onSubmit={submitHandler}>
                        <div className="form-group mb-3">
                            <label htmlFor="name" className="mb-2">Room Name</label>
                            <input
                                type="text"
                                onChange={(e) => setRoom({...room, name: e.target.value})}
                                className="form-control"
                                id="name"
                                placeholder="Room Name"
                                required />
                        </div>
                        <div className="form-group mb-3">
                            <div className="row">
                                <div className="col-md-6">
                                    <label htmlFor="type" className="mb-2">Type</label>
                                    <select 
                                        id="type"
                                        className="form-control"
                                        name="type"
                                        onChange={(e) => setRoom({...room, type: e.target.value})}
                                        required>
                                        <option value="">Choose Room</option>
                                        <option value="REGULAR">Regular</option>
                                        <option value="VIP">VIP</option>
                                    </select>
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="price" className="mb-2">Price</label>
                                    <input type="number"
                                        className="form-control"
                                        id="price" min="1"
                                        placeholder="Price"
                                        onChange={(e) => setRoom({...room, price: e.target.value})}
                                        required />
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default FormAddRoom;