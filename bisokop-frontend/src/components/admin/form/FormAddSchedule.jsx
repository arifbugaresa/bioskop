import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import moment from 'moment';
import { getData, postData } from '../../../service/Fetch';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    FormGroup,
    Label,
    Input,
    Form,
    Row,
    Col
} from 'reactstrap';

const FormAddSchedule = (props) => {

    const [modal, setModal] = useState(false);

    const token = useSelector(state => state.auth.token);

    const toggle = () => setModal(!modal);

    const [movies, setMovies] = useState([]);

    const [rooms, setRooms] = useState([]);

    const [schedule, setSchedule] = useState({
        roomId: '',
        movieId: '',
        showTime: '',
        showDate: ''
    })

    async function getDataMovies() {
        await getData(`admin/movie`, token)
            .then(res => {
                setMovies(res.data);
            })
    }

    async function getDataRooms() {
        await getData(`admin/room`, token)
            .then(res => {
                setRooms(res.data);
            })
    }

    useEffect(() => {

        async function setupData() {
            await getDataRooms();
            await getDataMovies();
        }

        setupData();

    }, [])

    const submitHandler = (event) => {
        event.preventDefault();

        postData("admin/schedule", schedule, token)
            .then(res => {
                props.setResponse(res.data)
                swal({
                    title: "Success!",
                    text: "Success Added Schedule",
                    icon: "success",
                })
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button color="success" onClick={toggle} size="sm">Add Schedule</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Add Schedule</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="movie" className="mb-2">Select Movie</Label>
                            <Input
                                type="select"
                                name="movie"
                                id="movie"
                                className="mb-3"
                                onChange={(e) => setSchedule({ ...schedule, movieId: e.target.value })}
                                required>
                                <option value="">Choose Movie</option>
                                {
                                    movies.map(movie =>
                                        <option key={movie.id} value={movie.id}>{movie.name}</option>
                                    )
                                }
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="room">Select Room</Label>
                            <select
                                name="room"
                                id="room"
                                className="mb-3 form-control"
                                onChange={(e) => setSchedule({ ...schedule, roomId: e.target.value })}
                                required>
                                <option value="">Choose Room</option>
                                {
                                    rooms.map(room =>
                                        <option value={room.id}>{room.name}</option>
                                    )
                                }
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <Row>
                                <Col md="5">
                                    <Label for="time" className="mb-2">Show Time</Label>
                                    <Input
                                        type="select"
                                        name="time"
                                        id="time"
                                        className="mb-3"
                                        onChange={(e) => setSchedule({ ...schedule, showTime: e.target.value })}
                                        required>
                                        <option value="">Select Time</option>
                                        <option value="10:00">10:00</option>
                                        <option value="13:00">13:00</option>
                                        <option value="15:00">15:00</option>
                                        <option value="18:00">18:00</option>
                                        <option value="21:00">21:00</option>
                                    </Input>
                                </Col>
                                <Col md="7">
                                    <Label for="duration" className="mb-2">Show Date</Label>
                                    <input
                                        type="date"
                                        min= {moment(Date.now()).format("YYYY-MM-DD")}
                                        name="duration"
                                        id="duration"
                                        className="form-control mb-2"
                                        onChange={(e) => setSchedule({ ...schedule, showDate: e.target.value })}
                                        required />
                                </Col>
                            </Row>
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default FormAddSchedule;