import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Col, Form, FormGroup, Input, Label, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { postData } from '../../../service/Fetch';
import '../../../assets/css/formAddMovies.css'
import swal from 'sweetalert';

const FormAddMovie = (props) => {
    const [modal, setModal] = useState(false);

    const token = useSelector(state => state.auth.token);

    const [image, setImage] = useState({ image: '', imagePreviewUrl: '' })

    const toggle = () => {
        setModal(!modal)
        setImage({ image: '', imagePreviewUrl: '' })

    }

    const [movie, setMovie] = useState({
        name: '',
        description: '',
        dataImage: '',
    })

    const handleImageChange = (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            setImage({
                image: file,
                imagePreviewUrl: reader.result
            });

            setImage64(reader.result)
        }

        reader.readAsDataURL(file)

    }

    const setImage64 = (imagePreview) => {
        let data = imagePreview.split(',')[1];
        let raw = window.atob(data);
        let rawLength = raw.length;

        let array = new Uint8Array(new ArrayBuffer(rawLength));
        for (let i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }

        let imageBlob = [];
        for (let i = 0; i < array.length; i++) {
            imageBlob.push(array[i]);
        }

        setMovie({ ...movie, dataImage: imageBlob })
    }

    const submitHandler = (event) => {

        event.preventDefault();

        postData("admin/movie", movie, token)
            .then(res => {
                props.setResponse(res.data);
                swal({
                    title: "Success!",
                    text: "Success Added Movie",
                    icon: "success",
                })
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            })

        toggle();
    }

    return (
        <div>
            <Button color="success" onClick={toggle} size="sm">Add Movie</Button>
            <Modal size="lg" isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Add Movie</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Row>
                                <Col md="8">
                                    <Label for="name" className="mb-2">Title</Label>
                                    <Input type="text" name="name" id="name" className="mb-2"
                                        onChange={(e) => setMovie({ ...movie, name: e.target.value })}
                                        required />
                                </Col>
                                <Col md="4">
                                    <Label for="dureation" className="mb-2">Duration</Label>
                                    <Input type="text" name="duration" id="duration" className="mb-2"
                                        value="2 Hours" disabled />
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Row>
                                <Col lg="8">
                                    <Label for="description" className="mb-2">Description</Label>
                                    <Input type="textarea" name="description" id="description"
                                        className="mb-2" style={{ height: 300 }}
                                        onChange={(e) => setMovie({ ...movie, description: e.target.value })}
                                        required />
                                </Col>
                                <Col lg="4">
                                    <Label for="image" className="mb-2">Image</Label>
                                    <Row>
                                        <Input type="file" name="image" id="image"
                                            onChange={(e) => handleImageChange(e)}
                                            required />
                                    </Row>
                                    <Row>
                                        {
                                            image.imagePreviewUrl &&
                                            <div>
                                                <Col className="mt-4 mb-2">
                                                    <img className="image-preview" src={image.imagePreviewUrl} alt="preview" />
                                                </Col>
                                            </div>
                                        }
                                    </Row>
                                </Col>
                            </Row>
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default FormAddMovie;