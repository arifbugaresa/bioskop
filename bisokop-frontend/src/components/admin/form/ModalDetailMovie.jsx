import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';

const ModalDetailMovie = (props) => {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <div>
            <Button color="primary" size="sm" className="text-white action-movie" onClick={toggle}><i class="bi bi-card-list"></i></Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Detail Movie</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name" className="mb-2">Title</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                value={props.movie.name}
                                readOnly/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="duration" className="mb-2">Duration</Label>
                            <Input type="text" name="duration" id="duration" className="mb-2"
                                value={props.movie.duration}
                                readOnly/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="description" className="mb-2">Description</Label>
                            <Input type="textarea" name="description" id="description"
                                className="mb-2" style={{ height: 200 }}
                                value={props.movie.description}
                                readOnly/>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Back</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ModalDetailMovie;