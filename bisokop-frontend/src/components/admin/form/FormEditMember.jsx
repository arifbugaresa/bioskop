import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import { 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    Form, 
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';
import { postData } from '../../../service/Fetch';

const FormEditMember = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => {
        setModal(!modal);
    }

    const token = useSelector(state => state.auth.token);

    const [newMember, setNewMember] = useState({
        id: props.user.id,
        name: props.user.name,
        username: props.user.username,
        address: props.user.address
    });

    const submitHandler = (event) => {

        event.preventDefault();

        postData(`admin/update-member`, newMember, token)
            .then(res => {
                props.setResponse(res.data)
                swal({
                    title: "Success!",
                    text: "Success Edit Data",
                    icon: "success",
                })
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button size="sm" color="warning" className="btn-detail-user" onClick={toggle}><i class="bi bi-pencil-square"></i></Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Edit User</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="name" className="mb-2">Name</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setNewMember({ ...newMember, name: e.target.value })}
                                value={newMember.name}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="username" className="mb-2">Username</Label>
                            <Input type="text" name="username" id="username" className="mb-2"
                                onChange={(e) => setNewMember({ ...newMember, username: e.target.value })}
                                value={newMember.username}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="address" className="mb-2">Address</Label>
                            <Input type="text" name="address" id="address" className="mb-2"
                                onChange={(e) => setNewMember({ ...newMember, address: e.target.value })}
                                value={newMember.address}
                                required />
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div >
    );
}

export default FormEditMember;