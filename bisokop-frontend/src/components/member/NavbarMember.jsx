import React, { useState } from 'react';
import '../../assets/css/navbarMember.css'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { authAction } from '../../redux/reducer/AuthReducer';

const NavbarMember = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [role] = useSelector((state) => state.auth.role);
    const dispatch = useDispatch();

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar className="navbar-default" light expand="md">
                <Container>
                    <NavbarBrand>
                        <Link to="/member" className="navbar-items">
                            NEXMOVI
                        </Link>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar className="navbar-collapse-member">
                        <Nav navbar>
                            <div className="navbar navbar-items">
                                <Link to="/member/movies" className="navbar navbar-items">
                                    Movies
                                </Link>
                            </div>
                        </Nav>
                        <Nav navbar>
                            <div>
                                {role === undefined ?
                                    <Link to="/login" className="navbar navbar-items float-right">
                                        Login
                                    </Link> :
                                    <UncontrolledDropdown className="navbar navbar-items float-right">
                                        <DropdownToggle nav caret className="navbar-items">
                                            Account
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem>
                                                <Link to="/member/account" className="navbar navbar-items">
                                                <span className="bi bi-person"> Account</span>
                                                </Link>
                                            </DropdownItem>
                                            <DropdownItem>
                                                <Link to="/member/booking-history" className="navbar navbar-items">
                                                    <span className="bi bi-journal-text"> Booking History</span>
                                                </Link>
                                            </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>
                                                <a
                                                    href="#!"
                                                    className="navbar navbar-items"
                                                    onClick={() => dispatch(authAction.logout())}>
                                                    <span className="bi bi-box-arrow-left"> Logout</span>
                                                </a>
                                            </DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                }
                            </div>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}

export default NavbarMember;