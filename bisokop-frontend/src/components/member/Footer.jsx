import React from 'react';
import '../../assets/css/footerMember.css'

const FooterMember = () => {
    return (
        <footer className="footer">
            <p className="text-center">© 2021 NEXMOVI</p>
        </footer>

    );
}

export default FooterMember;