import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Container, Row, Button } from 'reactstrap';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { useState } from 'react';
import { useEffect } from 'react';
import { getData } from '../../../service/Fetch';
import '../../../assets/css/movieRegDetail.css'

const SeatVipOrder = () => {

    const orderan = useLocation();

    const token = useSelector(state => state.auth.token);

    const o = orderan.state.data;

    const [seats, setSeats] = useState([]);

    async function fetchTicket() {
        let response = await getData(`ticket-member/${o.id}`, token)
        return response.data
    }

    async function setupSeat() {
        let listSeats = []

        for (let i = 1; i <= 6; i++) {
            let seatLoop = {
                id: "U" + i,
                name: "U" + i,
                status: "unreserved"
            }

            listSeats.push(seatLoop);
        }

        for (let i = 1; i <= 6; i++) {
            let seatLoop = {
                id: "D" + i,
                name: "D" + i,
                status: "unreserved"
            }

            listSeats.push(seatLoop);
        }

        return listSeats;
    }

    useEffect(() => {

        async function combineSeat() {
            let reservedSeats = await fetchTicket();

            let staticSeats = await setupSeat();

            staticSeats.map(staticSeat =>
                reservedSeats.map((reservedSeat) => {
                    if (staticSeat.id === reservedSeat.seat.id) {
                        staticSeat.status = reservedSeat.status
                    }
                })
            )

            setSeats(staticSeats);
        }

        combineSeat()

    }, [])

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>{o.roomName}</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item"><Link to="/member/booking-history">Booking History</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Seats</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row>
                <Col>
                    <p>{o.name}</p>
                </Col>
                <Col>
                    <p className="d-flex justify-content-end">{o.showTime} | {o.showDate}</p>
                </Col>
            </Row>
            <Row className="d-flex justify-content-center mt-5 mb-5">
                <Col lg="8">
                    <span className="d-block text-center p-2 bg-secondary text-white">Layar</span>
                </Col>
            </Row>
            <Row>
                {
                    seats.slice(0, 6).map(seat =>
                        seat.status === "unreserved" ?
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="secondary">{seat.name}</Button>
                            </Col> :
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="success" disabled>{seat.name}</Button>
                            </Col>
                    )
                }
            </Row>
            <Row>
                {
                    seats.slice(6, 12).map(seat =>
                        seat.status === "unreserved" ?
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="secondary">{seat.name}</Button>
                            </Col> :
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="success" disabled>{seat.name}</Button>
                            </Col>
                    )
                }
            </Row>
            <Row>
                <Col><h6>Informasi</h6></Col>
            </Row>
            <Row className="mb-2 mt-2">
                <Col className="text-center" sm="1"><Button color="success" disabled>U2</Button></Col>
                <Col sm="11"><p>Booked</p></Col>
            </Row>
            <Row>
                <Col className="d-flex justify-content-center mt-5 mb-5">
                    <Link to="/member/booking-history">
                        <Button color="secondary"> <span className="bi bi-arrow-left-circle"> Back</span></Button>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}

export default SeatVipOrder;