import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import { Container, Button, Row, Col } from 'reactstrap';
import { getData, postData } from '../../../service/Fetch';
import JumbotronMovie from './jumbotronMovie';
import CurrencyFormat from 'react-currency-format';
import { transactionAction } from '../../../redux/reducer/TransactionReducer';
import FooterMember from '../Footer';
import '../../../assets/css/movieRegDetail.css'

const MovieRegDetail = () => {

    // update
    const [, updateState] = React.useState();

    const forceUpdate = React.useCallback(() => updateState({}), []);

    // checkout

    const dispatch = useDispatch();

    const processCheckout = useSelector((state) => state.transaction.processCheckout);

    const user = useSelector((state) => state.auth.user);

    const history = useHistory();

    const [totalTicket, setTotalTicket] = useState(0);

    const [totalPrice, setTotalPrice] = useState(0);

    const movie = useLocation();

    const token = useSelector(state => state.auth.token);

    const [seats, setSeats] = useState();

    const [isLoading, setIsLoading] = useState(true);

    async function fetchTicket() {
        let response = await getData(`ticket/${movie.state.data.id}`, token)
        return response.data
    }

    async function setupSeat() {
        let listSeats = []

        for (let i = 1; i <= 25; i++) {
            let seatLoop = {
                id: "L" + i,
                name: "L" + i,
                status: "unreserved",
                clicked: false
            }

            listSeats.push(seatLoop);
        }

        for (let i = 1; i <= 25; i++) {
            let seatLoop = {
                id: "R" + i,
                name: "R" + i,
                status: "unreserved",
                clicked: false
            }

            listSeats.push(seatLoop);
        }

        return listSeats;
    }

    async function combineSeat() {
        let reservedSeats = await fetchTicket();
        let staticSeats = await setupSeat();

        staticSeats.map(staticSeat =>
            reservedSeats.map((reservedSeat) => {
                if (staticSeat.id === reservedSeat.seat.id) {
                    staticSeat.status = reservedSeat.status
                }
            })
        )

        return staticSeats;
    }

    const clickedHandler = (seat) => {
        let statusClicked = !seat.clicked

        let seatArr = seats;
        seatArr.map(s => {
            if (s.id === seat.id) {
                s.clicked = statusClicked;
            }
        })

        setSeats(seatArr);

        // hitung total price
        let ticket = 0;
        seats.map(s => {
            if (s.clicked === true) {
                ticket = ticket + 1;
            }
        })

        setTotalTicket(ticket);

        let ticketPrice = ticket * movie.state.data.price;

        setTotalPrice(ticketPrice);

        forceUpdate();
    }

    async function setupCheckout() {
        let seatClicked = [];

        await seats.map(s => {
            if (s.clicked === true) {
                seatClicked.push(s.id)
            }
        })

        let checkout = {
            idSchedule: movie.state.data.id,
            idUser: user.id,
            listSeat: seatClicked
        }

        dispatch(transactionAction.checkout(true));

        return checkout;
    }

    const checkoutHandler = async () => {

        let checkout = await setupCheckout();

        await postData("admin/orderan/checkout", checkout, token)
            .then(res => {
                dispatch(transactionAction.checkout(false));
                swal({
                    title: "Success!",
                    text: "Success checkout order",
                    icon: "success",
                });
            })
            .then(() => {
                history.push("/member/booking-history")
            })
            .catch(err => {
                dispatch(transactionAction.checkout(false));
                swal("Error", err.response.data, "error");
            });
    }

    useEffect(() => {

        async function setSeatCombined() {
            let seatCombined = await combineSeat()
            setSeats(seatCombined);
            setIsLoading(false);
        }

        setSeatCombined();

    }, [])

    return (
        <>
            <Container >
                <Row className="mt-4 mb-4">
                    <Col>
                        <h5>Detail Movie</h5>
                    </Col>
                    <Col className="flex-column align-items-end d-none d-md-flex">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                                <li className="breadcrumb-item"><Link className="breadcrumb-item-link" to="/member/movies">Movies</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Detail</li>
                            </ol>
                        </nav>
                    </Col>
                </Row>
                <Row>
                    <JumbotronMovie movie={movie.state.data} />
                </Row>
                {
                    !processCheckout ?
                        <>
                            {/* seat section */}
                            <Row className="d-flex justify-content-center mt-5 mb-5">
                                <Col lg="8">
                                    <span className="d-block text-center p-2 bg-secondary text-white">Layar</span>
                                </Col>
                            </Row>
                            <Row>
                                {
                                    isLoading ?
                                        <div className="d-flex justify-content-center mb-5">
                                            <div className="text-center mb-5">
                                                <div className="spinner-border mb-3 mt-5" role="status">
                                                    <span className="visually-hidden">Loading...</span>
                                                </div>
                                                <div className="mb-5">
                                                    <h5>Loading...</h5>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <>
                                            <Col>
                                                <Row>
                                                    {
                                                        seats.slice(0, 5).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row>
                                                    {
                                                        seats.slice(5, 10).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row>
                                                    {
                                                        seats.slice(10, 15).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row>
                                                    {
                                                        seats.slice(15, 20).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row>
                                                    {
                                                        seats.slice(20, 25).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                            </Col>
                                            <Col>
                                                <Row className="d-flex justify-content-end">
                                                    {
                                                        seats.slice(25, 30).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row className="d-flex justify-content-end">
                                                    {
                                                        seats.slice(30, 35).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row className="d-flex justify-content-end">
                                                    {
                                                        seats.slice(35, 40).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row className="d-flex justify-content-end">
                                                    {
                                                        seats.slice(40, 45).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                                <Row className="d-flex justify-content-end">
                                                    {
                                                        seats.slice(45, 50).map(seat =>
                                                            seat.status === "unreserved" ?
                                                                seat.clicked ?
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            color="primary"
                                                                            onClick={() => clickedHandler(seat)}
                                                                        >
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                    :
                                                                    <Col lg="2" className="mb-3">
                                                                        <Button className="btn-seat"
                                                                            size="lg"
                                                                            onClick={() => clickedHandler(seat)}
                                                                            color="secondary">
                                                                            {seat.name}
                                                                        </Button>
                                                                    </Col>
                                                                :
                                                                <Col lg="2" className="mb-3">
                                                                    <Button className="btn-seat" size="lg" color="danger" disabled>{seat.name}</Button>
                                                                </Col>
                                                        )
                                                    }
                                                </Row>
                                            </Col>
                                        </>
                                }
                            </Row>
                            {/* end seat section */}

                            <Row className="mt-3">
                                <Col sm="12" md="6">
                                    <Row className="mt-4">
                                        <Col><h5>Informasi</h5></Col>
                                    </Row>
                                    <Row className="mb-2 mt-2">
                                        <Col sm="2" className="text-center"><Button color="secondary">L1</Button></Col>
                                        <Col sm="10"><p>unreserved</p></Col>
                                    </Row>
                                    <Row className="mb-2 mt-2">
                                        <Col className="text-center" sm="2"><Button color="danger" disabled>L2</Button></Col>
                                        <Col sm="10"><p>reserved</p></Col>
                                    </Row>
                                    <Row className="mb-2 mt-2">
                                        <Col className="text-center" sm="2"><Button color="primary">L3</Button></Col>
                                        <Col sm="10"><p>Choosed</p></Col>
                                    </Row>
                                </Col>
                                <Col sm="12" md="6">
                                    <Row className="mt-4 mb-5">
                                        <h5>Orders Detail</h5>
                                        <Col md="7">
                                            <form>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="name"
                                                        className="col-sm-3 col-form-label">
                                                        Name
                                                    </label>
                                                    <div
                                                        className="col-sm-8 ">
                                                        <input
                                                            value={user.name}
                                                            type="text"
                                                            readOnly
                                                            className="form-control-plaintext test"
                                                            id="name" />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="email"
                                                        className="col-sm-3 col-form-label">
                                                        Email
                                                    </label>
                                                    <div
                                                        className="col-sm-9">
                                                        <input
                                                            value={user.email}
                                                            type="text"
                                                            readOnly
                                                            className="form-control-plaintext"
                                                            id="email" />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="phone"
                                                        className="col-sm-3 col-form-label">
                                                        Phone
                                                    </label>
                                                    <div
                                                        className="col-sm-6">
                                                        <input
                                                            value={user.phone_number}
                                                            type="text"
                                                            readOnly
                                                            className="form-control-plaintext"
                                                            id="phone" />
                                                    </div>
                                                </div>
                                            </form>
                                        </Col>
                                        <Col md="5">
                                            <form>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="totalSeat"
                                                        className="col-sm-6 col-form-label">
                                                        Total Ticket
                                                    </label>
                                                    <div
                                                        className="col-sm-6">
                                                        <input
                                                            value={totalTicket}
                                                            type="text"
                                                            readOnly
                                                            className="form-control-plaintext"
                                                            id="totalSeat" />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="totalPrice"
                                                        className="col-sm-6 col-form-label">
                                                        Total Price
                                                    </label>
                                                    <div
                                                        className="col-sm-6">
                                                        <CurrencyFormat
                                                            value={totalPrice}
                                                            displayType={"text"}
                                                            thousandSeparator={true}
                                                            prefix={"Rp. "}
                                                            renderText={(value) => (
                                                                <input
                                                                    value={value}
                                                                    type="text"
                                                                    readOnly
                                                                    className="form-control-plaintext"
                                                                    id="totalPrice" />
                                                            )}
                                                        />
                                                    </div>
                                                </div>
                                            </form>
                                            <Row className="mt-5">
                                                {
                                                    totalTicket === 0 ?
                                                        <Button color="primary" disabled>Checkout</Button> :
                                                        <Button color="primary" onClick={checkoutHandler} >Checkout</Button>
                                                }
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </> :
                        <>
                            <Row className="mt-5">
                                <Col>
                                    <h5>Process Checkout</h5>
                                </Col>
                            </Row>
                            <div className="d-flex justify-content-center mb-5">
                                <div className="text-center mb-5">
                                    <div className="spinner-border mb-3 mt-5" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                    <div className="mb-5">
                                        <h5>Loading...</h5>
                                    </div>
                                </div>
                            </div>
                        </>
                }
            </Container>
            <FooterMember />
        </>
    );
}

export default MovieRegDetail;