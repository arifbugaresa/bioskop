import React from 'react';
import '../../../assets/css/movieMember.css'
import { useHistory } from 'react-router';
import moment from 'moment';
import {
    Card, CardImg,
    CardBody,
    CardSubtitle,
    CardText,
} from 'reactstrap';

const MovieMember = (props) => {

    const history = useHistory();

    const scheduleDetailHandler = () => {

        props.schedule.type === "REGULAR" ?
        history.push(`movies-regular/${props.schedule.id}`, { data: props.schedule }) :
        history.push(`movies-vip/${props.schedule.id}`, { data: props.schedule })
    };

    return (
        <div>
            <Card 
                className="mb-4" 
                onClick={scheduleDetailHandler}>
                <CardImg 
                    className="card-movies" 
                    src={`data:image/*;base64, ` + props.schedule.dataImage} />
                <CardBody>
                    <CardSubtitle
                        tag="h6"
                        className="mb-2 movie-name">
                        {props.schedule.name}
                    </CardSubtitle>
                    <CardText>
                        {props.schedule.showTime.substring(0,5)}
                        <br />
                        {
                            props.schedule.roomName
                        }
                        <br />
                        {moment(props.schedule.showDate).format('LL')}
                    </CardText>
                </CardBody>
            </Card>
        </div>
    );
};

export default MovieMember;