import React, { useState } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { useSelector } from 'react-redux';
import '../../../assets/css/contentAccountMember.css'
import { postData } from '../../../service/Fetch';
import { useDispatch } from 'react-redux';
import { authAction } from '../../../redux/reducer/AuthReducer';
import swal from 'sweetalert';
import FooterMember from '../Footer';

const ContentAcountMember = () => {

    const user = useSelector((state) => state.auth.user);

    const token = useSelector(state => state.auth.token);

    const [editable, setEditable] = useState(false);

    const dispatch = useDispatch();

    const [userEdited, setUserEdited] = useState({
        id: user.id,
        name: user.name,
        username: user.username,
        address: user.address
    })

    const editHandler = (e) => {
        e.preventDefault();
        setEditable(!editable);
    }

    const submitHandler = (e) => {
        e.preventDefault();

        swal({
            title: "Are you sure?",
            text: "Are you sure you want to change your profile?",
            icon: "info",
            buttons: true,
        })
            .then((willConfirm) => {
                if (willConfirm) {
                    requestUpdateProfile();
                }
            });

        setEditable(!editable);
    }

    const requestUpdateProfile = () => {
        postData("user/update-profile", userEdited, token)
            .then(res => {
                if (res.data.username !== user.username) {
                    swal("Success", "Username changed, please login again!", "success");
                    dispatch(authAction.logout());
                } else {
                    swal("Success", "Success update data", "success");
                    dispatch(authAction.updateProfile(res.data));
                }
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });
    }

    return (
        <>
            <Container className="container-account">
                <Row className="pt-4">
                    <Col md="4" className="d-none d-lg-block">
                        <div className="sidebar-account">
                            <h6>Personal Information</h6>
                            <hr />
                        </div>
                    </Col>
                    <Col md="8">
                        <div className="content-account">
                            <h5>Personal Information</h5>
                            <form onSubmit={submitHandler}>
                                <div className="form-group mt-3 mb-2 row">
                                    <label htmlFor="name" className="col-sm-2 col-form-label">Name</label>
                                    <div className="col-sm-10">
                                        {editable ?
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="name"
                                                value={userEdited.name}
                                                onChange={(e) => setUserEdited({ ...userEdited, name: e.target.value })}
                                            /> :
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="name"
                                                value={user.name}
                                                disabled />
                                        }
                                    </div>
                                </div>
                                <div className="form-group mt-3 mb-2 row">
                                    <label htmlFor="username" className="col-sm-2 col-form-label">Username</label>
                                    <div className="col-sm-10">
                                        {editable ?
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="username"
                                                value={userEdited.username}
                                                onChange={(e) => setUserEdited({ ...userEdited, username: e.target.value })}
                                            /> :
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="username"
                                                value={user.username}
                                                disabled />
                                        }
                                    </div>
                                </div>
                                <div className="form-group mt-3 mb-2 row">
                                    <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="email"
                                            value={user.email}
                                            disabled />
                                    </div>
                                </div>
                                <div className="form-group mt-3 mb-2 row">
                                    <label htmlFor="number" className="col-sm-2 col-form-label">Phone</label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="number"
                                            value={user.phone_number}
                                            disabled />
                                    </div>
                                </div>
                                <div className="form-group mt-3 mb-2 row">
                                    <label htmlFor="address" className="col-sm-2 col-form-label">Address</label>
                                    <div className="col-sm-10">
                                        {editable ?
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="address"
                                                value={userEdited.address}
                                                onChange={(e) => setUserEdited({ ...userEdited, address: e.target.value })}
                                            /> :
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="address"
                                                value={user.address}
                                                disabled />
                                        }
                                    </div>
                                </div>
                                <div className="form-group mt-3 mb-2 row">
                                    {
                                        editable === true ?
                                            <div className="col-sm-12 d-flex justify-content-end">
                                                <button type="button" onClick={(e) => editHandler(e)} className="btn btn-danger cancel-button">Cancel</button>
                                                <button type="submit" className="btn btn-primary">Submit</button>
                                            </div> :
                                            <div className="col-sm-12 d-flex justify-content-end">
                                                <button type="button" onClick={(e) => editHandler(e)} className="btn btn-secondary edit-button">Edit</button>
                                            </div>
                                    }
                                </div>
                            </form>
                        </div>
                    </Col>
                </Row>
            </Container >
            <FooterMember/>
        </>
    );
}

export default ContentAcountMember;