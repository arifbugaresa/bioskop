import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Col, Container, Row } from 'reactstrap';
import '../../../assets/css/contentHomeMember.css';
import { getData } from '../../../service/Fetch';
import FooterMember from '../Footer';
import HomeMovieMember from './HomeMovieMember';

const ContentHomeMember = () => {

    const [schedules, setSchedules] = useState([]);

    const token = useSelector((state) => state.auth.token);

    useEffect(() => {
        getData(`member/schedule-today`, token)
            .then(res => {
                setSchedules(res.data);
            })
    }, [])

    return (
        <>
            <Row>
                <Carousel>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 img-banner"
                            src="assets/images/epic-movie-banner.jpg"
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 img-banner"
                            src="assets/images/aquaman-movie-banner.jpg"
                            alt="Second slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 img-banner"
                            src="assets/images/httyd-movie-banner.jpg"
                            alt="Third slide"
                        />
                    </Carousel.Item>
                </Carousel>
            </Row>
            <div className="body-member">
                <Container>
                    <Row className="pt-4 mb-4">
                        <Col className="mt-3">
                            <h4 className="text-white">Today</h4>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        {
                            schedules.length !== 0 ?
                                schedules.map(schedule =>
                                    <Col lg="3">
                                        <HomeMovieMember schedule={schedule} />
                                    </Col>
                                ) :
                                <p className="text-white">Empty Movies Today</p>
                        }
                    </Row>
                </Container>
            </div>
            <FooterMember />
        </>
    );
}

export default ContentHomeMember;