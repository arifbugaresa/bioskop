import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';
import { getData } from '../../../service/Fetch';
import MovieMember from '../content/MovieMember';
import FooterMember from '../Footer';
import '../../../assets/css/contentMoviesMember.css'
import '../../../assets/css/contentSeat.css'

const ContentMoviesMember = () => {

    const [schedules, setSchedules] = useState([]);

    const token = useSelector((state) => state.auth.token);

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        number: 0,
        totalPages: 0,
    });

    const [page, setPage] = useState(0);

    const [keyword, setKeyword] = useState("");

    const [finalKeyword, setFinalKeyword] = useState("");

    useEffect(() => {

        const url = `member/schedule-paginated?pageNo=${page}&keyword=${finalKeyword}`

        getData(url, token)
            .then(res => {
                setSchedules(res.data.content);
                setPaginated({
                    number: res.data.number,
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [page, finalKeyword])

    const searchHandler = () => {
        setFinalKeyword(keyword);
    }

    const resetHandler = () => {
        setFinalKeyword("");
        setKeyword("");
    }

    return (
        <>
            <Container>
                <Row className="mt-4">
                    <Col className="flex-column align-items-end d-none d-md-flex">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link className="breadcrumb-item-link" to="/admin"><i class="bi bi-house"></i></Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Movies</li>
                            </ol>
                        </nav>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-end">
                    <Col className="d-flex align-items-center" sm="6" md="6" lg="9">
                        <h5>Movies</h5>
                    </Col>
                    <Col sm="6" md="6" lg="3">
                        <div className="mb-4 mt-3 d-flex justify-content-center">
                            
                            <input
                                type="text"
                                className="form-control input-search"
                                value={keyword}
                                placeholder="Search Movie ..."
                                name="key"
                                onChange={(e) => setKeyword(e.target.value)}
                            />
                            {
                                keyword !== "" &&
                                <button
                                    className="btn btn-sm btn-danger btn-search"
                                    onClick={resetHandler}
                                >
                                    <span><i class="bi bi-x-square"></i></span>
                                </button>
                            }
                            <button
                                className="btn btn-sm btn-primary btn-search"
                                onClick={searchHandler}
                            >
                                Search
                            </button>
                        </div>
                    </Col>
                </Row>
                <Row className="content-movie">
                    {
                        schedules.length !== 0 ?
                            schedules.map(schedule =>
                                <Col lg="3">
                                    <MovieMember schedule={schedule} />
                                </Col>
                            ) :
                            <p>Movies Empty...</p>
                    }
                </Row>

                {/* Pagination */}
                <Row className="mt-4 mb-5">
                    <Col className="d-flex justify-content-center">
                        <nav>
                            {
                                paginated.empty ?
                                    <p></p> :
                                    <ul className="pagination">
                                        {
                                            paginated.first ?
                                                <li className="page-item disabled">
                                                    <span className="page-link">Previous</span>
                                                </li> :
                                                <li
                                                    onClick={() => setPage(paginated.number - 1)}
                                                    className="page-item">
                                                    <span className="page-link">Previous</span>
                                                </li>
                                        }
                                        {
                                            Array.from(Array(paginated.totalPages), (event, index) => {
                                                return (
                                                    <li
                                                        onClick={() => setPage(index)}
                                                        className={index === page ? "page-item active" : "page-item"}>
                                                        <span className="page-link">
                                                            {index + 1}
                                                        </span>
                                                    </li>
                                                );
                                            })
                                        }
                                        {
                                            paginated.last ?
                                                <li className="page-item disabled">
                                                    <span className="page-link">Next</span>
                                                </li> :
                                                <li
                                                    onClick={() => setPage(paginated.number + 1)}
                                                    className="page-item">
                                                    <span className="page-link">Next</span>
                                                </li>
                                        }
                                    </ul>
                            }
                        </nav>
                    </Col>
                </Row>
            </Container>
            <FooterMember />
        </>
    );
}

export default ContentMoviesMember;