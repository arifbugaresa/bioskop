import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col, Container, Row, Table, Button } from 'reactstrap';
import { getData } from '../../../service/Fetch';
import DetailBooking from '../modal/DetailBooking';
import { useHistory } from 'react-router';
import '../../../assets/css/contentHistoryMember.css'

const ContentHistoryMember = () => {

    const [myOrders, setMyOrders] = useState();

    const token = useSelector(state => state.auth.token);

    const user = useSelector((state) => state.auth.user);

    const [page, setPage] = useState(0);

    let history = useHistory();

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    useEffect(() => {
        const url = `member/orderans?id=${user.id}&pageNo=${page}&pageSize=5`

        getData(url, token)
            .then(res => {
                setMyOrders(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [page])

    const seatHandler = (orderan) => {
        // cek regular atau vip
        orderan.roomType === "VIP" ?
            history.push(`seat-vip/${orderan.id}`, { data: orderan }) :
            history.push(`seat-regular/${orderan.id}`, { data: orderan })
    }

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>History Order</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Order</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>OrderID</th>
                                <th>Order Date</th>
                                <th>Ticket</th>
                                <th>Movie Name</th>
                                <th>Show Time</th>
                                <th>Show Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                myOrders !== undefined ?
                                    myOrders.length !== 0 ?
                                        myOrders.map(or =>
                                            <tr key={or.id}>
                                                <td>{or.id}</td>
                                                <td>{or.orderDate}</td>
                                                <td>{or.totalTicket} Ticket</td>
                                                <td>{or.name}</td>
                                                <td>{or.showTime.substring(0, 5)}</td>
                                                <td>{or.showDate}</td>
                                                <td>
                                                    <p
                                                        size="sm"
                                                        className={
                                                            or.orderStatus === 'requested' ?
                                                                "order-requested" : "order-accepted"
                                                        }
                                                        disabled="true">
                                                        {or.orderStatus}
                                                    </p>
                                                </td>
                                                <td>
                                                    <DetailBooking order={or} />
                                                    <Button
                                                        className="btn-seat-order"
                                                        onClick={() => seatHandler(or)}
                                                        color="success"
                                                        size="sm">
                                                        Seat
                                                    </Button>
                                                </td>
                                            </tr>
                                        ) :
                                        <tr>
                                            <td colSpan="8" className="text-center">Data Kosong</td>
                                        </tr>
                                    :
                                    <p>Fetching data ...</p>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {/* Pagination */}
            <Row className="mt-4 mb-5">
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(paginated.number - 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(paginated.number + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentHistoryMember;