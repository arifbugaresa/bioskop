import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
    processCheckout: false,
    processApprove: false
}

const transactionSlice = createSlice({
    name: "transaction",
    initialState: initialAuthState,
    reducers: {
        checkout(state, action) {
            state.processCheckout = action.payload;
        },
        approve(state, action) {
            state.processApprove = action.payload;
        }
    }
})

export const transactionAction = transactionSlice.actions;

export default transactionSlice.reducer;