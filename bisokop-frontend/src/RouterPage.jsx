import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import DashboardAdmin from './pages/admin/DashboardAdmin';
import LoginPage from './pages/LoginPage'
import MemberPage from './pages/member/MemberPage';
import RegisterPage from './pages/RegisterPage';

const RouterPage = () => {
    const token = useSelector((state) => state.auth.token);
    const role = useSelector((state) => state.auth.role);

    const AdminRoute = ({...props}) => {
        return (
            (token !== '' && role === 'ROLE_ADMIN') ? <Route {...props} /> : <Redirect to="/login" />
        )
    }

    const MemberRoute = ({...props}) => {
        return (
            (token !== '' && role === 'ROLE_MEMBER') ? <Route {...props} /> : <Redirect to="/login" />
        )
    }

    const CostumRoute = ({...props}) => {
        return (
            token ? <Redirect to={role === "ROLE_ADMIN" ? "/admin" : "/member"} /> : <Route {...props} />
        )
    }

    return (
        <Switch>
            <Route exact path="/"><Redirect to="/member" /></Route>
            <CostumRoute exact path="/login" component={LoginPage} />
            {/* <Route exact path="/login" component={LoginPage} /> */}
            <Route exact path="/register" component={RegisterPage} />
            <MemberRoute path="/member" component={MemberPage} />
            <AdminRoute path="/admin" component={DashboardAdmin} />
        </Switch>
    );
}

export default RouterPage;