import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/registerPage.css'
import swal from 'sweetalert';
import { postNoRestrict } from '../service/Fetch';
import { useHistory } from 'react-router';


const RegisterPage = () => {

    let history = useHistory();

    const [data, setData] = useState({
        name: '',
        username: '',
        password: '',
        address: '',
        phone_number: '',
        email: ''
    });

    const registerHandler = (e) => {

        e.preventDefault();

        postNoRestrict("member/register", data)
            .then(res => {
                swal({
                    title: "Success!",
                    text: "Registration successfully, please login",
                    icon: "success",
                });
                history.push("/login")
            })
            .catch(err => {
                swal("Error", err.response.data.password, "error");
            })
    }

    return (
        <div className="body-background">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8 col-lg-6 mt-2">
                        <div className="card-custom mt-5 trans">
                            <div className="card-header bg-transparent mb-0 text-center">
                                <img src="assets/images/logo-nexmovi.png"
                                    alt="logo nexmovi"
                                    className="logo-login mt-3"
                                />
                                <p className="mt-2">Register to create your account</p>
                            </div>
                            <div className="card-body">
                                <form onSubmit={registerHandler}>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="name" className="mb-2">Full Name</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, name: e.target.value })}
                                                    required
                                                    placeholder="Full Name" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="username" className="mb-2">Username</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, username: e.target.value })}
                                                    required
                                                    placeholder="Username" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-3">
                                                <label htmlFor="email" className="mb-2">Email</label>
                                                <input
                                                    type="email"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, email: e.target.value })}
                                                    required
                                                    placeholder="Email" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group mt-3">
                                                <label htmlFor="phone" className="mb-2">Phone Number</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, phone_number: e.target.value })}
                                                    required
                                                    pattern="^(?=.\d.).{9,12}$"
                                                    title="Must be number 9-12 Digit"
                                                    placeholder="Phone Number" />
                                                <div class="invalid-tooltip">
                                                    Must be number 9-12 Digit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-3 mb-4">
                                                <label htmlFor="password" className="mb-2">Password</label>
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, password: e.target.value })}
                                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                                                    title="Minimum 6 Character, Uppercase, Lowercase, Number"
                                                    required
                                                    placeholder="Password" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group mt-3 mb-4">
                                                <label htmlFor="email" className="mb-2">Address</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, address: e.target.value })}
                                                    required
                                                    placeholder="Address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group d-flex justify-content-end">
                                        <input type="submit" value="Register" className="btn btn-register" />
                                    </div>
                                    <div>
                                        <p className="mt-2">Already have an account? <Link to="/login">Log in</Link></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default RegisterPage;