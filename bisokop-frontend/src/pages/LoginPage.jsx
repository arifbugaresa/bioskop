import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { authAction } from '../redux/reducer/AuthReducer';
import { postNoRestrict } from '../service/Fetch';
import swal from 'sweetalert';
import '../assets/css/loginPage.css'
import { Link } from 'react-router-dom';

const LoginPage = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    let history = useHistory();

    const loginHandler = (e) => {
        e.preventDefault();

        let data = {
            username: username,
            password: password
        }

        postNoRestrict("user/login", data)
            .then(res => {
                dispatch(authAction.login(res.data));
                res.data.role === "ROLE_MEMBER" ? history.push("/") : history.push("/admin")
            })
            .catch(err => {
                if (typeof err.response.data === "string") {
                    swal("Error", err.response.data, "error");
                }
            })
    }

    return (
        <div className="body-background">
            <div className="container">
                <div className="row justify-content-center pt-4 pb-5">
                    <div className="col-md-6 col-lg-4 mt-5 ">
                        <div className="card-custom mt-5 trans">
                            <div className="card-header bg-transparent mb-0 text-center">
                                <img src="assets/images/logo-nexmovi.png"
                                    alt=""
                                    className="logo-login mt-3"
                                />
                                <p className="mt-2">Log in to your account</p>
                            </div>
                            <div className="card-body">
                                <form onSubmit={loginHandler}>
                                    <div className="form-group mt-3 mb-3">
                                        <input
                                            type="text"
                                            className="form-control"
                                            onChange={(e) => setUsername(e.target.value)}
                                            placeholder="Username" />
                                    </div>
                                    <div className="form-group mt-3 mb-3">
                                        <input
                                            type="password" 
                                            className="form-control" 
                                            onChange={(e) => setPassword(e.target.value)}
                                            placeholder="Password" />
                                    </div>
                                    <div className="form-group text-center">
                                        <input type="submit" value="Login" className="btn btn-block btn-login form-control" />
                                    </div>
                                    <div>
                                        <p className="mt-3">Dont have account? <Link to="/register">register</Link></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoginPage;